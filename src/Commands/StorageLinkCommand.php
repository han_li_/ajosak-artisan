<?php
/**
 * Created by PhpStorm.
 * User: c5101
 * Date: 2017/8/16
 * Time: 下午 04:53
 */

namespace System;

use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StorageLinkCommand extends Command
{
    protected function configure ()
    {
        $this
            ->setName('storage:link')
            ->setDescription('Create a symbolic link from "public/storage" to "storage/app/public');
    }

    protected function execute (InputInterface $input, OutputInterface $output)
    {

        $Filesystem = new Filesystem();
        //$root = dirname(dirname(dirname(dirname(dirname(__DIR__)))));

        //  ln -s 原目錄 要建立軟連結的目錄
        $target =  'storage' . DIRECTORY_SEPARATOR . 'public';
        $link =  "public" . DIRECTORY_SEPARATOR . 'storage';

        if (is_dir($link) && !is_writable($link)) {
            $output->writeln('The "%s" directory is not writable');

            return;
        }

        if (!file_exists($link)) {


            if (!strtolower(substr(PHP_OS, 0, 3)) === 'win') {
                return symlink($target, $link);
            }

            $mode = $Filesystem->isDirectory($target) ? 'J' : 'H';

            exec("mklink /{$mode} \"{$link}\" \"{$target}\"");

            if (file_exists($link . DIRECTORY_SEPARATOR . 'log')) {
                $Filesystem->deleteDirectory($link . DIRECTORY_SEPARATOR . 'log');
            }

            $output->writeln("The [public/storage] directory has been linked.");
        } else {
            $output->writeln("The [public/storage] directory already Exists!");
        }
    }
}